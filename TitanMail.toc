## Interface: 100002
## Title: Titan Panel [|ccff7fff7Mail|r] |cff00aa00 v10.0.2.0|r
## Notes: A Titan Plugin to let you know you have mail.  Also remembers any Auction alerts.
## Author: Yunohu, Mikhailo, Skadi
## X-Date: 2020-10-30
## SavedVariables: TITAN_MAIL_SETTINGS
## Dependencies: Titan
## OptionalDeps: 
## Version: 10.0.2.0
TitanMail.xml
